# Food Tracker

## Description 

Excel workbook to track meals for a diet plan.

## Features
* food database with makro / micro nutrient included (DB sheet)
* select specific food for specifiy daytime
* fast-forward food tracking

## Getting started

* add the nutritian items into the Refernces sheet
* copy the week template sheet to a new week xy sheet
* select nutritians in the correspondig section
** add the amount in g for related food item
* add additional nutritian items with new rows (see + symbol)

## Updates

### 0.2

* added sheet "combination" to map meals from particular food items
* changed sheet DB to a database object
** fixed reference and data issues
* J.A.D.E. function code refactoring
* German translation switch for nutritian names
   * add new J.A.D.E function switch_language
   * German food names added to DB sheet

### 0.1

* added DB sheet
* added refernces sheet
* added food tracking table
* J.A.D.E. function 'calculate_nutriation_values' to track food values
* added parser script to add food item from fddb.info and open food facts

## Documentation
https://docs.microsoft.com/de-de/javascript/api

## Contact

benjamin.gabriel@outlook.de
