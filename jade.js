// global values
const g_maxLineNumber=2000
const g_numberOfFoodItems=250


// main function
async function calculate_nutritian_values(excel) {
  let lineIndex=1
  const settingsSheet = excel.workbook.worksheets.getItem("Settings");
  const languageValue = settingsSheet.getRange("A2")
  languageValue.load("values")
  await excel.sync();
  let language = languageValue.values[0].toString()
  
  const sheet = excel.workbook.worksheets.getActiveWorksheet();
  

  do {
    lineIndex++;
    let descriptionField = sheet.getRange("A" + lineIndex)
    let mealIdField = sheet.getRange("B" + lineIndex)
    let amountlField = sheet.getRange("C" + lineIndex)
    
    descriptionField.load("values");
    mealIdField.load("values");
    amountlField.load("values");
    await excel.sync();
    var descriptionFieldText = descriptionField.values[0].toString()
    
    if (mealIdField.values[0].toString() !== "" && 
      amountlField.values[0].toString() !== "") {
      
      // get object
      let foodObj = await getFoodObject(excel, mealIdField.values[0].toString()) 
        
      // langauge settings  
      const languageColumnId = await getLangaugeColumnId(excel, language);
        
      // calculate line depending on object  
      await calculateMeal(excel, amountlField.values[0], foodObj, lineIndex, languageColumnId)    
      }
      
    if (descriptionFieldText.trim().toUpperCase() === "TOTAL" || lineIndex > g_maxLineNumber) {      
      break;
    }
    
  } while (true)
  
  await excel.sync();
}

// helper function - find food object in db sheet
const getFoodObject = async (context, foodId) => {
  
  let sheetId = "DB"
  if (foodId.includes("Recept-")) {
      sheetId = "Combination"
  }
  
  const dbSheet = context.workbook.worksheets.getItem(sheetId);
  let foodIds = dbSheet.getRange("A2:A" + g_numberOfFoodItems)
  
  foodIds.load("values")
  await context.sync();
  
  let elem = foodIds.values.find(el => el[0].toString() === foodId)
  let foodIndexTableLine = foodIds.values.indexOf(elem) + 2
  
  let foodObj = dbSheet.getRange("B" + foodIndexTableLine + ":AH" + foodIndexTableLine)
  
  return foodObj
}


// helper function - 
const calculateMeal = async (context, amount, foodObj, lineIndex, languageColumnId) => {
  
  foodObj.load("values")
  await context.sync();
  
  // calcualte and add data
  const sheet = context.workbook.worksheets.getActiveWorksheet();
  
  await context.sync();
    sheet.getRange("D"+ lineIndex + ":E" + lineIndex).values=[[foodObj.values[0][languageColumnId], foodObj.values[0][2]]]
  

  
  const amountRelative = amount / 100
  
  sheet.getRange("F"+ lineIndex + ":M" + lineIndex).values=[[foodObj.values[0][3] * amountRelative, foodObj.values[0][4] * amountRelative, foodObj.values[0][5] * amountRelative, foodObj.values[0][6] * amountRelative, foodObj.values[0][7] * amountRelative, foodObj.values[0][8] * amountRelative, foodObj.values[0][9] * amountRelative * amountRelative, foodObj.values[0][10] * amountRelative]]
  
  sheet.getRange("N"+ lineIndex + ":U" + lineIndex).values=[[foodObj.values[0][11] * amountRelative, foodObj.values[0][12] * amountRelative, foodObj.values[0][13] * amountRelative, foodObj.values[0][14] * amountRelative, foodObj.values[0][15] * amountRelative, foodObj.values[0][16] * amountRelative, foodObj.values[0][17] * amountRelative, foodObj.values[0][18] * amountRelative]]
  
  sheet.getRange("V"+ lineIndex + ":AI" + lineIndex).values=[[foodObj.values[0][19] * amountRelative, foodObj.values[0][20] * amountRelative, foodObj.values[0][21] * amountRelative, foodObj.values[0][22] * amountRelative, foodObj.values[0][23] * amountRelative, foodObj.values[0][24] * amountRelative, foodObj.values[0][25] * amountRelative, foodObj.values[0][26] * amountRelative, foodObj.values[0][27] * amountRelative, foodObj.values[0][28] * amountRelative, foodObj.values[0][29] * amountRelative, foodObj.values[0][30] * amountRelative, foodObj.values[0][31] * amountRelative, foodObj.values[0][32] * amountRelative]]
}

// depending on the the langauge setting, column is determined
const getLangaugeColumnId = async (context, language) => {
  let colId = 0
  
  switch(language) {
    case "English": {
      colId = 0; 
      break;
    }
    case "German": {
      colId = 1; 
      break;
    }
    default: {
      colId = 0;
    }
  }
  await context.sync()
  return colId
}

// main function
async function compare_nutritian_values(excel) {
 
  const compareSheet = excel.workbook.worksheets.getItem("Comparison");
  const numberOfFoodItems = compareSheet.getRange("B1")
  const startIndexObject = compareSheet.getRange("D1")
  
  numberOfFoodItems.load("values")
  startIndexObject.load("values")
  await excel.sync();
 
 let index = parseInt(startIndexObject.values[0].toString())
 let temp = parseInt(numberOfFoodItems.values[0].toString())
 const finalRowCondition = temp + index
 
 while (true) {
  
      console.log("action index " + index)
      
    while (true) {
      
      let foodDataItem = compareSheet.getRange("A3" + index + ":AH" + index)
      foodDataItem.load("values")
      await excel.sync();
      
      index++
      
      if (foodDataItem.values[0][0].toString() === "") {
        console.log("break")
        break;
      }
    }
      
      if (index >= finalRowCondition) {
          break;
      }
 }
}

function setup() {
  show_html('<div style="border: 5px outset blue; padding:25px;"><h2 style="color:blue;">Weekly Report</h2><p>Add Values for all food inserted this week.</p> <button id="button-calc">Calculate</button><button id="button-compare">Compare</button></div>')
  
  tag("button-calc").onclick = jade.automate(calculate_nutritian_values)
  tag("button-compare").onclick = jade.automate(compare_nutritian_values)
}