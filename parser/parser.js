// HTML elements
const inputName = document.getElementById('foodName')
const inputGroup = document.getElementById('foodGroup')
const textArea = document.getElementById('textAreaNutritianContent');
const formButton = document.getElementById('formatButton');
const formButtonFoodstar = document.getElementById('formatButtonFoodstar');
const resultElement = document.getElementById('parsedResult')
const copyClipboardButton = document.getElementById('copyClipboardButton')

// Key to read
const fieldsMakrosFddb = ['Calorific value|Brennwert', 'Calories|Kalorien', 'Protein', 'Carbohydrates|Kohlenhydrate', 'thereof Sugar|davon Zucker', 'Fat|Fett', 'Dietary fibre|Ballaststoffe' ,'Water content']
const fieldsVitaminsFddb = ['Vitamin C' ,'Retinol|Vitamin A', 'Vitamin D', 'Vitamin E', 'Thiamine|Vitamin B1', 'Riboflavin|Vitamin B2', 'Vitamin B6', 'Vitamin B12']
const fieldsMineralsFddb = ['Salt|Salz', 'Iron|Eisen', 'Zinc|Zink', 'Magnesium', 'Chlorine|Chlorid', 'Manganese|Mangan', 'Sulphur|Schwefel', 'Potassium|Kalium', 'Calcium|Kalzium', 'Phosphorus|Phosphor', 'Copper|Kupfer', 'Fluorine|Fluorid', 'Iodine|Jod']
const fieldsMakrosFddbFoodstar = ['energy', 'kcal', 'protein', 'carbs', 'sugar', 'fat', 'saturated', 'salt']

// delimeter for CSV file
const delimeter = ','

// Parser function for fddb
function formatContentFddb() {
	
	const value = textArea.value.trim()
	var lines = value.split('\n');
	result = inputName.value + delimeter + inputGroup.value
	valueSet=false
	
	// Makro fields
	fieldsMakrosFddb.forEach(makro => {
		valueSet = false
		lines.forEach((line, index) => {
			
			if (makro.includes(line) && 
			!(lines[index + 1].includes('What does this chart show?')
			  || lines[index + 1].includes('Was zeigt diese Grafik?'))
			 ) {
				console.log(line)
				let value = lines[index + 1].replace(',','.')
				result += delimeter + value
				valueSet=true
				return
			}
		})
		
		if (!valueSet) {
			result += delimeter
		}
		
		// satureted fat is not available in fddb
		if (makro.includes('Fat')) {
			result += delimeter
		}
	})
	
	// Vitamin fields
	fieldsVitaminsFddb.forEach(vitamin => {
		valueSet = false
		lines.forEach((line, index) => {
			
			if (vitamin.includes(line)) {
				let value = lines[index + 1].replace(',','.')
				result += delimeter + value
				valueSet=true
				return
			}
		})
		
		if (!valueSet) {
			result += delimeter
		}
	})
	
	result += delimeter
	
	// Mineral fields
	fieldsMineralsFddb.forEach(mineral => {
		valueSet = false
		lines.forEach((line, index) => {
			
			if (mineral.includes(line)) {
				let value = lines[index + 1].replace(',','.')
				result += delimeter + value
				valueSet=true
				return
			}
		})
		
		if (!valueSet) {
			result += delimeter
		}
	})
	
	resultElement.innerHTML = result
	
}

// parser function for Foodstar
function formatContentFoodstar() {
	
	const value = textArea.value.trim()
	var lines = value.split('\n');
	result = inputName.value + delimeter + inputGroup.value
	
	fieldsMakrosFddbFoodstar.forEach(makro => {
		lines.forEach((line, index) => {
			if (line.includes(makro)) {
				
				let adaptedDelimiter = delimeter
				if (makro === 'salt') {
					adaptedDelimiter += delimeter + delimeter + delimeter + delimeter + delimeter + delimeter + delimeter + delimeter + delimeter
				}
				
				let resultValue = line.replace(makro, '').replaceAll(' ', '').replaceAll('\t', '')
				result += adaptedDelimiter + resultValue
				return
			}
		})
	})
	
	resultElement.innerHTML = result
}

// creates hidden textarea to copy text to clipboard
function copyTextToClipboard() {
	/* Get the text field */
	let copyText = resultElement.innerHTML
    let textArea = document.createElement("textarea");
    textArea.value = copyText;
    document.body.appendChild(textArea);
    textArea.select();
    document.execCommand("Copy");
    textArea.remove();
}

formButton.addEventListener('click', formatContentFddb);
formButtonFoodstar.addEventListener('click', formatContentFoodstar);
copyClipboardButton.addEventListener('click', copyTextToClipboard);